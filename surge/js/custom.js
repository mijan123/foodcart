jQuery(document).ready(function ($) {
	new WOW().init();
	$(".burger-menu").click(function (e) {
		e.preventDefault();

		$(".mobile-menu").toggleClass("active");
	});



	$(".home-slick-slider").slick({
		autoplay: true,
		dots: true,
		arrows: false,
		infinite: true,
		slidesToShow: 1,
		fade: true,

		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					dots: true,
					slidesToShow: 1,
					slidesToScroll: 1,
					infinite: true,

				}
			},
			{
				breakpoint: 991,
				settings: {
					slidesToShow: 1,
					centerMode: false,
					slidesToScroll: 1,
					vertical: false


				}
			},
			{
				breakpoint: 480,
				settings: {
					autoplay: false,
					vertical: false,
					centerMode: false,
					arrows: false,
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
	$(".offer-slider").slick({
		autoplay: true,
		dots: false,
		arrows: false,
		infinite: true,
		slidesToShow: 3,

		speed: 4000,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					dots: true,
					slidesToShow: 1,
					slidesToScroll: 1,
					infinite: true,

				}
			},
			{
				breakpoint: 991,
				settings: {
					slidesToShow: 1,
					centerMode: false,
					slidesToScroll: 1,
					vertical: false


				}
			},
			{
				breakpoint: 480,
				settings: {
					autoplay: true,
					vertical: false,
					centerMode: false,
					arrows: false,
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});

	$(".gallery-menu-slider").slick({
		autoplay: true,
		dots: false,
		arrows: false,
		infinite: true,
		slidesToShow: 5,

		speed: 2000,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					dots: true,
					slidesToShow: 1,
					slidesToScroll: 1,
					infinite: true,

				}
			},
			{
				breakpoint: 991,
				settings: {
					slidesToShow: 1,
					centerMode: false,
					slidesToScroll: 1,
					vertical: false


				}
			},
			{
				breakpoint: 480,
				settings: {
					autoplay: true,
					vertical: false,
					centerMode: true,
					dots: false,
					arrows: false,
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
	$(".client-slider").slick({
		autoplay: false,
		centerMode: true,
		arrows: true,
		infinite: false,
		slidesToShow: 3,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					dots: true,
					slidesToShow: 1,
					slidesToScroll: 1,
					infinite: true,

				}
			},
			{
				breakpoint: 991,
				settings: {
					slidesToShow: 1,
					centerMode: false,
					slidesToScroll: 1,
					vertical: false


				}
			},
			{
				breakpoint: 480,
				settings: {
					autoplay: false,
					vertical: false,
					centerMode: false,
					arrows: true,
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});


	jQuery(document).ready(function ($) {
		setTimeout(function () {
			$('.featured-wrapper-banner').addClass('loaded');
		}, 600);
		// ____trial-men


		//  $("a.trial-button").click(function(){
		//    $(".banner-trial-form").toggleClass("show");
		//  });
		// $(".user-support-form a.form-close").click(function(){
		//    $(".banner-trial-form").toggleClass("show");
		//  });
		//  $("a.call-button").click(function(){
		//    $(".call-form-wrapper").toggleClass("show");
		//  });
		// $(".trial-call-form a.form-close").click(function(){
		//    $(".call-form-wrapper").toggleClass("show");
		//  });
		//------- Wow JS Initialized --------// 


		//hide and show search

		$(window).on("scroll", function () {
			if ($(window).scrollTop() > 50) {
				$("header.site-header").addClass("fixed");
				$("header.site-header").css("background-color", "#fff");
				$("header.site-header").css("box-shadow", "0 2px 6px 0 rgba(0,0,0,.12), inset 0 -1px 0 0 #dadce0");
				$("header.site-header").css("padding-top", "10px");
				$("header.site-header").css("padding-bottom", "10px");

				$(".top-scroller").css("opacity", "1");
				$(".burger-menu span").css("background-color", "#1d7bea");
			} else {
				$("header.site-header").removeClass("fixed");
				$("header.site-header").css("background-color", "transparent");
				$("header.site-header").css("box-shadow", "none");
				$("header.site-header").css("padding-top", "50px");
				$("header.site-header").css("padding-bottom", "30px");


			}

		});

		// ----smooth-scrollint---
		$(document).on('click', 'a[href^="#core"] , a[href^="#faq"], a[href^="#contact"], a[href^="#feature"] ,a[href^="#price"] ,a[href^="#banner"]', function (event) {
			event.preventDefault();

			$('html, body').animate({
				scrollTop: $($.attr(this, 'href')).offset().top
			}, 1000);
		});

		// JS written by Themetide
		$('.counter').counterUp({
			delay: 10,
			time: 2000
		});

		//pizza
		$(".pizza-item-list ul li.ind-item").click(function (e) {
			e.preventDefault();

			$(this).toggleClass("added");
		});

		// mobilenav
		if ($('#nav-menu-container').length) {
			var $mobile_nav = $('#nav-menu-container').clone().prop({
				id: 'mobile-nav'
			});
			$mobile_nav.find('> ul').attr({
				'class': 'mobile-menu',
				'id': ''
			});
			$('body').append($mobile_nav);
			$('body').prepend('<button type="button" id="mobile-nav-toggle"><i class="fa fa-bars"></i></button>');
			$('body').append('<div id="mobile-body-overly"></div>');
			$('#mobile-nav').find('.menu-has-children').prepend('<i class="fa fa-angle-down"></i>');

			$(document).on('click', '.menu-has-children i', function (e) {
				$(this).next().toggleClass('menu-item-active');
				$(this).nextAll('ul').eq(0).slideToggle();
				$(this).toggleClass("fa-angle-up fa-angle-down");
			});

			$(document).on('click', '#mobile-nav-toggle', function (e) {
				$('body').toggleClass('mobile-nav-active');
				$('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
				$('#mobile-nav-toggle').toggleClass('cross');
				$('#mobile-body-overly').toggle();
			});

			$(document).on('click', function (e) {
				var container = $("#mobile-nav, #mobile-nav-toggle");
				if (!container.is(e.target) && container.has(e.target).length === 0) {
					if ($('body').hasClass('mobile-nav-active')) {
						$('body').removeClass('mobile-nav-active');
						$('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
						$('#mobile-body-overly').fadeOut();
					}
				}
			});
		} else if ($("#mobile-nav, #mobile-nav-toggle").length) {
			$("#mobile-nav, #mobile-nav-toggle").hide();
		}


		// showhide
		$('.pizza-chicken, .pizza-buff, .pizza-pork, .pizza-veg').hide();
		$('.pizza-empty').show();
		$('#choose-pizza').change(function () {
			var selected = $('#choose-pizza option:selected').text();
			$('.pizza-empty').toggle(selected == "none");
			$('.pizza-chicken').toggle(selected == "Chicken");
			$('.pizza-buff').toggle(selected == "Buff");
			$('.pizza-pork').toggle(selected == "Pork");
			$('.pizza-veg').toggle(selected == "Veg");
		});






		// $(window).load(function() {
		//   setTimeout(function() {
		//     $('#preloader').fadeOut(1000);
		//   }, 1000);
		//   setTimeout(function() {
		//     $('#preloader').remove();
		//   }, 1500);

	});

	// parallax-mousejs
	// $("#l2").mousemove(function(e) {
	//   parallaxIt(e, ".page-title", -50);
	//   parallaxIt(e, "title", -10);
	// });

	// function parallaxIt(e, target, movement) {
	//   var $this = $("#l2");
	//   var relX = e.pageX - $this.offset().left;
	//   var relY = e.pageY - $this.offset().top;

	//   TweenMax.to(target, 1, {
	//     x: (relX - $this.width() / 2) / $this.width() * movement,
	//     y: (relY - $this.height() / 2) / $this.height() * movement
	//   });
	// }
	// $(window).load(function() {
	//   setTimeout(function() {
	//     $('#preloader').fadeOut(1000);
	//   }, 1000);
	//   setTimeout(function() {
	//     $('#preloader').remove();
	//   }, 1500);

	// });

	// parallax-mousejs

});